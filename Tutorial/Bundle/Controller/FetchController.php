<?php

namespace Tutorial\Bundle\Controller;

use Symfony\Component\HttpFoundation\Response,
  eZ\Publish\Core\MVC\Symfony\Controller\Content\ViewController as APIViewController,
  eZ\Publish\API\Repository\Values\Content,
  eZ\Publish\API\Repository\Values\Content\Query,
  eZ\Publish\API\Repository\Values\Content\Query\Criterion,
  eZ\Publish\API\Repository\Values\Content\Search\SearchResult,
  eZ\Publish\API\Repository\Values\Content\Query\SortClause;

class FetchController extends APIViewController
{
  public function test(){
    return new Response("fetch controller test");
  }

  // from http://partialcontent.com/code/working-with-ez-publish-5-subrequests
  public function blogPosts($subTreeLocationId, $viewType='summary')
  {
    //Retrieve the location service from the Symfony container
    $locationService = $this->getRepository()->getLocationService();

    //Load the called location (node) from the repository based on the ID
    $root = $locationService->loadLocation( $subTreeLocationId );

    //Get the modification time from the content object
    $modificationDate = $root->contentInfo->modificationDate;
    
    //Retrieve a subtree fetch of the latest posts
    $postResults = $this->fetchSubTree(
      $root,
      array('blog_post'),
      array(new SortClause\DateModified(Query::SORT_DESC))
    );

    //Convert the results from a search result object into a simple array
    $posts = array();
    foreach ( $postResults->searchHits as $hit )
    {
      $posts[] = $hit->valueObject;

      //If any of the posts is newer than the root, use that post's modification date
      if ($hit->valueObject->contentInfo->modificationDate > $modificationDate) {
        $modificationDate = $hit->valueObject->contentInfo->modificationDate;
      }
    }

    //Set the etag and modification date on the response
    $response = $this->buildResponse(
      __METHOD__ . $subTreeLocationId,
      $modificationDate
    );

    //If nothing has been modified, return a 304
    if ( $response->isNotModified( $this->getRequest() ) )
    {
      //return $response;
    }

    //Render the output
    return $this->render(
      'TutorialBundle::posts_list.html.twig',
      array( 'posts' => $posts, 'viewType' => $viewType ),
      $response
    );
  }

  // from http://partialcontent.com/code/working-with-ez-publish-5-subrequests
  protected function fetchSubTree(
    \eZ\Publish\API\Repository\Values\Content\Location $subTreeLocation,
    array $typeIdentifiers=array(),
    array $sortMethods=array()
  )
  {

    //Access the search service provided by the eZ Repository (Public API)
    $searchService = $this->getRepository()->getSearchService();

    $criterion = array(
      new Criterion\ContentTypeIdentifier( $typeIdentifiers ),
      new Criterion\Subtree( $subTreeLocation->pathString )
    );

    //Construct a query
    $query = new Query();
    $query->criterion = new Criterion\LogicalAnd(
      $criterion
    );

    if ( !empty( $sortMethods ) )
    {
      $query->sortClauses = $sortMethods;
    }
    $query->limit = 20;

    //Return the content from the repository
    return $searchService->findContent( $query );
  }
}
