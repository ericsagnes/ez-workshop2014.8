# eZ Publish 5 勉強会 2014.8 「バンドル」

## バンドルの骨組みを作る

eZ Publishのルートから

    php ezpublish/console generate:bundle

Bundle namespace: Tutorial/Bundle  
Bundle name: そのままエンターキー  
Target directory: そのままエンターキー  
Configuration format: yml  
Do you want to generate the whole directory structure: yes  
Do you confirm generation: yes  

そのあとはeZのルートのsrcフォルダーにバンドルフォルダーが作成されます。

## バンドル構造

├── Controller  コントローラファイル  
├── DependencyInjection   依存管理  
├── Resources テンプレート、翻訳、ドキュメンテーション、設定ファイル  
├── Tests テスト  
└── Twig  Twig拡張  

## コントローラバンドル

デフォルトにコントローラバンドルがあります。

ソースファイル: Tutorial/Bundle/Controller/DefaultController.php

ルーティング設定:

    tutorial_homepage:
        path:     /hello/{name}
        defaults: { _controller: TutorialBundle:Default:index }

コントローラソース:

Tutorial/Bundle/Controller/DefaultController.php

コントローラ確認: 

ドメイン/hello/world

利用されるテンプレート:

Tutorial/Bundle/Resources/views/Default/index.html.twig

## デザインバンドル

参考URL: http://partialcontent.com/code/working-with-ez-publish-5-templates

新しいページレイアウトテンプレートを作成: Tutorial/Bundle/Resources/views/pagelayout.html.twig

設定ファイルに新しいページレイアウトを設定

ezpublish/config/parameters.yml

    parameters:
        ....
        #ezpublish_legacy.default.view_default_layout: 'eZDemoBundle::pagelayout.html.twig'
        ezpublish_legacy.default.view_default_layout: 'TutorialBundle::pagelayout.html.twig'

## TWIG拡張バンドル

参考URL: http://symfony.com/doc/current/cookbook/templating/twig_extension.html

TWIGクラスファイルを作成: Tutorial/Bundle/Twig/PriceExtension.php

サービスを登録する必要があります:

Tutorial/Bundle/Resources/config/services.yml

    services:
      tutorial.twig.price_extension:
        class: Tutorial\Bundle\Twig\PriceExtension
        tags:
          - { name: twig.extension }

Tutorial/Bundle/Resources/views/pagelayout.html.twig に新フィルターのテストコードをいれます:

    <p>{{ '5000'|price }}</p>

## フェッチコントローラ

参考URL: http://partialcontent.com/code/working-with-ez-publish-5-subrequests 

コントローラファイルを作成: Tutorial/Bundle/Controller/FetchController.php

コントローラをサービスに登録:

Tutorial/Bundle/Resources/config/services.yml

    services:
      ....
      tutorial.fetch_controller:
        class: Tutorial\Bundle\Controller\FetchController
        arguments: [@ezpublish.view_manager]
        calls: 
          - [setContainer, [@service_container] ]

関連テンプレートを作成:

* Tutorial/Bundle/Resources/views/posts_list.html.twig
* Tutorial/Bundle/Resources/views/summary/blog_post.html.twig


pagelayoutに追加: Tutorial/Bundle/Resources/views/pagelayout.html.twig

    {{ render( controller( "tutorial.fetch_controller:blogPosts", { 'subTreeLocationId': 90 } ) ) }}

課題: summaryカスタムビューはうまく利用されない（lineビューモードでも、legacyのテンプレートは利用されます）。





